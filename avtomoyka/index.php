<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Автомойка");
?>
<div class="main-page-block-type-1 washing-block">
	<div class="col-6">
		<div class="page-info">
			<div class="page-title">
				<h1>Автомойка</h1>
				<hr>
			</div>
			<div class="page-description">
				<p>
					 Автомоечный центр CORE12 – это большой технологичный комплекс премиум-класса в центре Москвы.
				</p>
				<p>
					 Бережно помоем ваш автомобиль, защитим лакокрасочное покрытие с использованием прогрессивных методик и средств. Вы сможете оценить безупречный сервис, индивидуальный подход и профессионализм команды центра, а также с комфортом провести время в клиентской зоне комплекса.
				</p>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="advantages-block">
			<div class="advantages-title">
				<i class="icon plus-icon"></i>
				<span>Преимущества</span>
			</div>
			<ul class="advantages-list">
				<li>
					<i class="icon speed-icon"></i>
					<span>Квалифицированные специалисты</span>
				</li>
				<li>
					<i class="icon marker-icon"></i>
					<span>Удобное расположение</span>
				</li>
				<li>
					<i class="icon car-icon"></i>
					<span>Современное оборудование</span>
				</li>
				<li>
					<i class="icon gamepad-icon"></i>
					<span>Комфортная клиентская зона</span>
				</li>
				<li>
					<i class="icon bush-icon"></i>
					<span>Индивидуальный подход</span>
				</li>
				<li>
					<i class="icon cup-icon"></i>
					<span>Гарантия качества</span>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="additional-block row">
	<div class="col-6">
		<div class="services-block">
			<div class="services-block-title">
				 Услуги и цены
			</div>
			 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"uslugi",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "avtomoyk",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("PRICE",""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
			<div class="text-right show-more">
				 <a class="btn btn-default" target="_blank" href="https://docs.google.com/spreadsheets/d/19lCQfkxt16XTqTQjESDnrJDsI0Il_GB6QBOajwJ3aUw/edit?ts=5bb1e61e#gid=463689828">Все услуги</a>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="sales-block">
			<div class="sales-block-title">
				 Акции
			</div>
					 <?$APPLICATION->IncludeComponent(
					"bitrix:news.list",
					"akcii-slider-type-1",
					Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "Y",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"DISPLAY_DATE" => "N",
						"DISPLAY_NAME" => "Y",
						"DISPLAY_PICTURE" => "Y",
						"DISPLAY_PREVIEW_TEXT" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"FIELD_CODE" => "",
						"FILTER_NAME" => "",
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",
						"IBLOCK_ID" => "4",
						"IBLOCK_TYPE" => "content",
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
						"INCLUDE_SUBSECTIONS" => "Y",
						"MESSAGE_404" => "",
						"NEWS_COUNT" => "20",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "",
						"PARENT_SECTION" => "5",
						"PARENT_SECTION_CODE" => "avtomoyk",
						"PREVIEW_TRUNCATE_LEN" => "",
						"PROPERTY_CODE" => array("",""),
						"SET_BROWSER_TITLE" => "N",
						"SET_LAST_MODIFIED" => "N",
						"SET_META_DESCRIPTION" => "N",
						"SET_META_KEYWORDS" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"SORT_BY1" => "ACTIVE_FROM",
						"SORT_BY2" => "SORT",
						"SORT_ORDER1" => "DESC",
						"SORT_ORDER2" => "ASC",
						"STRICT_SECTION_CHECK" => "N"
					)
				);?>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
