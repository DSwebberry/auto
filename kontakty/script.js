var directionsService, directionsDisplay,
	coordinates = {lat: 55.778007, lng: 37.646428};
var styles = [
	{
		"elementType": "geometry.fill",
		"stylers": [
			{
				"color": "#f7f7f7"
			}
		]
	},
	{
		"elementType": "labels.icon",
		"stylers": [
			{
				"saturation": -100
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"elementType": "labels.text",
		"stylers": [
			{
				"saturation": -100
			}
		]
	},
	{
		"featureType": "administrative",
		"stylers": [
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "geometry.stroke",
		"stylers": [
			{
				"color": "#6f6f6f"
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels",
		"stylers": [
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "labels.icon",
		"stylers": [
			{
				"saturation": -100
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#ffffff"
			},
			{
				"weight": 2
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry.stroke",
		"stylers": [
			{
				"color": "#e4e4e4"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels",
		"stylers": [
			{
				"saturation": -100
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "road.highway",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#616161"
			}
		]
	},
	{
		"featureType": "transit",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#a2a2a2"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "geometry",
		"stylers": [
			{
				"color": "#c7c7c7"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "labels.text.fill",
		"stylers": [
			{
				"color": "#9e9e9e"
			}
		]
	}
];
var popup, Popup;
$(document).ready(function(){
	$(document).on("click", ".js-create-route", function(){
		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
		}
		else{
			getGeolocationFromIpInfo();
		}
	});
});

function initMap(){
	definePopupClass();
	directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer();
	var popupContent = document.getElementById('popupContent'),
		zoom = 15,
		map = new google.maps.Map(document.getElementById('mapContainer'), {
			center: coordinates,
			zoom: zoom,
			disableDefaultUI: true,
			scrollwheel: true
		});

	directionsDisplay.setMap(map);
	map.setOptions({styles: styles});


	popup = new Popup(
		new google.maps.LatLng(coordinates.lat, coordinates.lng),
		popupContent);
	popup.setMap(map);

}
function definePopupClass() {
	Popup = function(position, content) {
		this.position = position;

		content.classList.add('popup-bubble-content');

		var pixelOffset = document.createElement('div');
		pixelOffset.classList.add('popup-bubble-anchor');
		pixelOffset.appendChild(content);

		this.anchor = document.createElement('div');
		this.anchor.classList.add('popup-tip-anchor');
		this.anchor.appendChild(pixelOffset);

		this.stopEventPropagation();
	};
	Popup.prototype = Object.create(google.maps.OverlayView.prototype);

	Popup.prototype.onAdd = function() {
		this.getPanes().floatPane.appendChild(this.anchor);
	};

	Popup.prototype.onRemove = function() {
		if (this.anchor.parentElement) {
			this.anchor.parentElement.removeChild(this.anchor);
		}
	};

	Popup.prototype.draw = function() {
		var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
		// Hide the popup when it is far out of view.
		var display =
			Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
				'block' :
				'none';

		if (display === 'block') {
			this.anchor.style.left = divPosition.x + 'px';
			this.anchor.style.top = divPosition.y + 'px';
		}
		if (this.anchor.style.display !== display) {
			this.anchor.style.display = display;
		}
	};

	Popup.prototype.stopEventPropagation = function() {
		var anchor = this.anchor;
		anchor.style.cursor = 'auto';

		['dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
			'pointerdown']
			.forEach(function(event) {
				anchor.addEventListener(event, function(e) {
					e.stopPropagation();
				});
			});
	};
}
function successCallback(r){
	var lat = r.coords.latitude;
	var lng = r.coords.longitude;
	var myLatlng = {lat: lat, lng: lng};
	createRoute(myLatlng, coordinates)
}
function errorCallback(e){
	getGeolocationFromIpInfo();
}
function getGeolocationFromIpInfo(){
	$.get("https://ipinfo.io", function (response) {
		var myCoordinates = response.loc.split(',');
		var myLatlng = {lat: parseFloat(myCoordinates[0]), lng: parseFloat(myCoordinates[1])};
		createRoute(myLatlng, coordinates);
	}, "jsonp");
}
function createRoute(myLatlng, endCoordinate){
	var request = {
		origin: myLatlng,
		destination: endCoordinate,
		travelMode: 'DRIVING'
	};
	directionsService.route(request, function (response, status) {
		if (status == 'OK') {
			directionsDisplay.setDirections(response);
		} else {
			console.log(status);
		}
	});
}


