<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="main-menu">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>

		<li>
			<a
					<?if($arItem["LINK"] == "#open-additional-menu"):?>
						target-menu="<?=$arItem["LINK"]?>" target="_blank"
					<?else:?>
						href="<?=$arItem["LINK"]?>"

					<?endif;?>
					class="menu-item"><?=$arItem["TEXT"]?></a>
			<?if($arItem["LINK"] == "#open-additional-menu"):?>
				<ul class="second-menu">
					<li>
						<a href="https://docs.google.com/spreadsheets/d/1eszwA-PWmX76s_qPPY12AT_m5wDT0gYfBWJgRfUhDHA/edit?ts=5bb1e656#gid=42171286">Детейлинг</a>
					</li>
					<li>
						<a href="https://docs.google.com/spreadsheets/d/19lCQfkxt16XTqTQjESDnrJDsI0Il_GB6QBOajwJ3aUw/edit?ts=5bb1e61e#gid=463689828">Автомойка</a>
					</li>
					<li>
						<a href="https://docs.google.com/spreadsheets/d/1LX9lgpDCXcdtkyIyhWezjJhELSt99gg62goFtF_jNF8/edit?ts=5bb1e63c#gid=1618341297">Шиномонтаж</a>
					</li>
				</ul>
			<?endif;?>
		</li>

	
<?endforeach?>

</ul>
<?endif?>
