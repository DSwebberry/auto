<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/twenty_twenty/css/twentytwenty.css");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/twenty_twenty/css/twentytwenty-no-compass.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/twenty_twenty/js/jquery.event.move.js");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/twenty_twenty/js/jquery.twentytwenty.js");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.carousel.css");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.theme.default.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/owlcorousel/js/owl.carousel.js");
$this->setFrameMode(true);
?>
<?
if(CModule::IncludeModule("iblock")):
	$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'SECTION_ID'=>$arResult["ID"]);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
	$i_line = 0;
	?>
	<section class="portfolio-section">
		<div class="main-panel ">
			<div class="view-detail-image-block">
				<div class="loader bg-grey absolute"><div class="spin"></div></div>
				<div class="twenty-twenty"></div>
			</div>
		</div>
		<div class="right-panel">
			<div class="category-list">
				<div class="owl-carousel">
					<div class="category-sub-list">
					<?while($ar_result = $db_list->GetNext()):?>
						<?$i_line++?>
						<?if(!($i_line%11)):?>
							</div>
							<div class="category-sub-list">
						<?endif;?>
						<div class="category-item" data-iblock-id="<?=$arParams["IBLOCK_ID"]?>" data-section-id="<?=$ar_result['ID']?>">
							<div class="caret">
								<i class="fa fa-angle-right"></i>
							</div>

							<span><?=$ar_result['NAME']?></span>
						</div>

					<?endwhile;?>
					</div>
				</div>
				<div id="custom-nav" class="owl-nav">
					<div class="counter"></div>
				</div>
			</div>
			<div class="preview-list">

			</div>
		</div>
	</section>

<?endif;?>


