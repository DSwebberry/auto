<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
if(!CModule::IncludeModule("iblock"))die();


call_user_func($_POST["METHOD"], $_POST["PARAMS"]);

function getPhotoByCategory($arParams){
	$arResult = [];
	$arFilter = Array(
		'IBLOCK_ID'=>$arParams["iblock_id"],
		'SECTION_ID'=>$arParams["category_id"]);
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

	while($ob = $res->GetNextElement()){
		$arProps = $ob->GetProperties();
		$arFields = $ob->GetFields();
		$photoBeforeUtl = CFile::GetPath($arProps["PHOTO_BEFORE"]["VALUE"]);
		$photoAfterUtl = CFile::GetPath($arProps["PHOTO_AFTER"]["VALUE"]);
		$arResult[] = [
			"id"=>$arFields["ID"],
			"name"=>$arFields["NAME"],
			"before_image"=>$photoBeforeUtl,
			"after_image"=>$photoAfterUtl
		];
	}
	AJAX_response("success", $arResult);
}

function AJAX_response($status = null, $data = null, $message = null){
	echo json_encode(["status"=>$status, "data"=>$data, "message"=>$message]);
}




