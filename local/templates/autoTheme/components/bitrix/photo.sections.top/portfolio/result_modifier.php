<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["TD_WIDTH"] = round(100/$arParams["LINE_ELEMENT_COUNT"])."%";
$arResult["nRowsPerItem"] = 2; //Image and Name
$arResult["bDisplayFields"] = count($arParams["FIELD_CODE"])>0;

foreach($arResult["SECTIONS"] as $section){
	if($section["CODE"] == $arParams["FILTER_NAME"]["PARENT_SECTION_CODE"]){
		$arResult = $section;
		continue;
	}
}
?>
