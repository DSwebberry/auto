var url = "/local/templates/autoTheme/components/bitrix/photo.sections.top/portfolio/template_ajax.php";
$(document).ready(function(){
	var $section = $(".portfolio-section");
	var $categoryList = $(".category-list .owl-carousel");
	var $previewList = $(".preview-list");



	$categoryList.owlCarousel({
		loop:false,
		margin:20,
		nav:true,
		dots:false,
		mouseDrag:true,
		touchDrag:true,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		navContainer: "#custom-nav",
		onInitialized: owlAfterInit,
		items:1,
	});

	$('.owl-carousel').on('changed.owl.carousel', function(e) {

		$section.find('.counter').text(++e.item.index + '/' + $(".category-list .owl-carousel .owl-item").length)
	});


	$(document).on("click", ".category-item", function(){
		$previewList.html('<div class="loader show  absolute"><div class="spin"></div></div>');
		var $this = $(this);
		$this.parent().find(".category-item").removeClass("active");
		$this.addClass("active");
		var category_id = $(this).data("section-id");
		var iblock_id = $(this).data("iblock-id");

		$.ajax({
			url: url,
			type: "POST",
			data:{
				METHOD:"getPhotoByCategory",
				PARAMS:{
					category_id:category_id,
					iblock_id: iblock_id,
				}
			},
			success: function(response){
				response = JSON.parse(response);
				$previewList.html(renderImageList(response.data));
				$previewList.find(".owl-carousel").owlCarousel({
					loop:false,
					margin:20,
					nav:false,
					dots:true,
					mouseDrag:true,
					touchDrag:true,
					items:1,
				});
				$previewList.find(".preview-item").first().trigger("click");
				$previewList.find(".loader").remove();
			}
		});
	});

	$(document).on("click", ".preview-item", function() {
		$section.find(".view-detail-image-block .loader").toggleClass("show");
		$section.find(".view-detail-image-block .twenty-twenty").empty();
		var $this = $(this);
		var imageBeforeUrl = $this.data("before-image-url");
		var imageAfterUrl = $this.data("after-image-url");
		$section.find(".view-detail-image-block .twenty-twenty").append(renderDetailImage(imageBeforeUrl, "onloadBeforeImg"));
		$section.find(".view-detail-image-block .twenty-twenty").append(renderDetailImage(imageAfterUrl))
	});
	$(".category-sub-list").find(".category-item").first().trigger("click");

});
function onloadBeforeImg(){
	$(".portfolio-section").find(".view-detail-image-block .twenty-twenty").twentytwenty({
		before_label: 'До',
		after_label: 'После',
		no_overlay: false,
		move_with_handle_only: true,
		click_to_move: false,
	});
	setTimeout(function(){
		$(".portfolio-section").find(".view-detail-image-block .loader").toggleClass("show");

	}, 300);
}
function owlAfterInit(event){
	$('.counter').text('1/' + this.items().length);
	$(".category-list .owl-carousel").find(".category-item").first().trigger("click");
}
function renderDetailImage(url, callfunc){
	var func = "";
	if(callfunc){
		func = 'onload="'+callfunc+'()"';
	}
	return '<img src="'+url+'" '+func+'>';
}
function renderImageList(data){
	var	i_line = 1;
	var html = "";
	html += '<div class="image-list-by-category owl-carousel">'+
			'<div class="image-sublist-by-category">';
	$.each(data, function(i, item){
		html += '<div class="preview-item" ' +
			'data-before-image-url="'+item.before_image+'" ' +
			'data-after-image-url="'+item.after_image+'" ' +
			'style="background-image:url(\''+item.after_image+'\')"></div>';
		if(!(i_line%12)) {
			html += '</div>'+
			'<div class= "image-sublist-by-category">';
		}
		i_line++;
	});
	html += '</div>' +
		'</div>';
	return html;
}
