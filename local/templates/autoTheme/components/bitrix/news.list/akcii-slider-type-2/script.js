$(document).ready(function(){
	$('.sales-list .owl-carousel').owlCarousel({
		loop:true,
		nav:true,
		dots:false,
		margin:0,
		items:1,
		navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
		navContainer: "#sales-custom-nav",
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		autoplaySpeed: 7000,
	});
});
