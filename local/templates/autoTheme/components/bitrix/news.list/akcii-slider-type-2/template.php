<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.carousel.css");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.theme.default.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/owlcorousel/js/owl.carousel.js");
$this->addExternalJS("/script.js");
$this->setFrameMode(true);
?>
<div class="sales-list">
	<div id="sales-custom-nav" class="owl-nav"></div>
	<div class="owl-carousel">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			$itemImage = null;
			if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])){
				$itemImage = $arItem["PREVIEW_PICTURE"]["SRC"];
			}
			?>
			<div class="sales-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="background-image: url('<?=$itemImage ? $itemImage: ""?>');">
				<div class="sales-body">
					<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
						<div class="sale-title"><?echo $arItem["NAME"]?></div>
					<?endif;?>
					<?if($arItem["DETAIL_TEXT"]):?>
						<div class="sale-description"><?echo $arItem["DETAIL_TEXT"]?></div>
					<?endif;?>
				</div>

			</div>
		<?endforeach;?>
	</div>
</div>
