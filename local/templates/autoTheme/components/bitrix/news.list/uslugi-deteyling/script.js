$(document).ready(function(){
	var $ancherList = $(".services-list-description");

	$(window).scroll(function () {
		if ($(this).scrollTop() > $(".advantages-and-services").height() && !$ancherList.hasClass("fixed")) {
			$ancherList.addClass("fixed");

		} else if ($(this).scrollTop() <= $(".advantages-and-services").height() && $ancherList.hasClass("fixed")) {
			$ancherList.removeClass("fixed");
		}
	});
	$(document).on("click", ".ancher", function () {
		event.preventDefault();
		$('html, body').animate({ scrollTop: $('#'+$(this).data("id")).offset().top-100}, 500);
	});

});
