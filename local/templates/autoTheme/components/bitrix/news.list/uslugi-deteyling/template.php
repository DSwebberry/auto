<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJS("/script.js");

?>
<div class="section-title" id="<?=$arResult["SECTION"]["PATH"][1]["CODE"]?>">
	<?=$arResult["SECTION"]["PATH"][1]["NAME"]?>
</div>
<ul class="services-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

		<li class="service-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="col-6">
				<div class="info-block">
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<div class="service-title"><?echo $arItem["NAME"]?></div>
				<?endif;?>
				<div class="service-description">
					<?echo $arItem["DETAIL_TEXT"]?>
				</div>
				<div class="property-list">
					<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
						<div class="property-item">
							<?=$arProperty["NAME"]?>:&nbsp;
							<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
								<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
							<?else:?>
								<?=$arProperty["DISPLAY_VALUE"];?>
							<?endif?>
						</div>
					<?endforeach;?>
				</div>
				<div class="controll-panel">
					<button class="btn btn-info open-call-me-popup"
							data-title="<?=$arItem["NAME"]?>"
							data-price="<?=$arItem["PROPERTIES"]["PRICE"]["VALUE"]?>"
							data-differ="<?=$arItem["PROPERTIES"]["STOYKOST"]["VALUE"]?>">
						Заказать
					</button>
				</div>
			</div>
			</div>
			<div class="col-6">
				<div class="services-photo-section">
				<?$arIDs = $arItem["PROPERTIES"]["PHOTOS"]["VALUE"];
				global $arNewsFilter;
				if (count ($arIDs) > 0){
					$arNewsFilter = Array ("ID" => $arIDs);
				}
				?>
				<?$APPLICATION->IncludeComponent(
					"bitrix:photo.section",
					"service_before_after_gallery",
					Array(
						"ADD_SECTIONS_CHAIN" => "N",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"BROWSER_TITLE" => "-",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_SORT_FIELD" => "sort",
						"ELEMENT_SORT_ORDER" => "asc",
						"FIELD_CODE" => array("",""),
						"FILTER_NAME" => "arNewsFilter",
						"IBLOCK_ID" => "3",
						"IBLOCK_TYPE" => "content",
						"LINE_ELEMENT_COUNT" => "20",
						"MESSAGE_404" => "",
						"META_DESCRIPTION" => "-",
						"META_KEYWORDS" => "-",
						"PAGER_BASE_LINK_ENABLE" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Фотографии",
						"PAGE_ELEMENT_COUNT" => "20",
						"PROPERTY_CODE" => array("PHOTO_BEFORE","PHOTO_AFTER"),
						"SECTION_CODE" => "deteyling",
						"SECTION_ID" => "8",
						"SECTION_URL" => "",
						"SECTION_USER_FIELDS" => array("",""),
						"SET_LAST_MODIFIED" => "N",
						"SET_STATUS_404" => "N",
						"SET_TITLE" => "N",
						"SHOW_404" => "N",
						"PARENT_ID"=>$arItem["ID"],
						"ARR_IDS"=>$arIDs,
					)
				);?>
			</div>
			</div>
		</li>
	<?endforeach;?>
</ul>
