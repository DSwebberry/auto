$(document).ready(function(){

	var myslider = $('.sales-list').bxSlider({
		mode:"horizontal",
		slideMargin:40,
		adaptiveHeight:false,
		controls: true,
		pager: true,
		nextText:"",
		prevText:"",
		shrinkItems: true,
		infiniteLoop: true,
		maxSlides:1,
		// slideWidth:70,
		auto: true,
		autoControls: true,
		stopAutoOnClick: true,
		autoStart: true,
		ticker: true,
		tickerSpeed: 5000,
		speed: 7000,
	});
});
