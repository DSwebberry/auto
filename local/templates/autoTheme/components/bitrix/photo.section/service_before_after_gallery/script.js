$(document).ready(function(){

	var $corousels = $('.services-photogallery.owl-carousel')
	$corousels.each(function(i, item){
		$(item).owlCarousel({
			loop:false,
			margin:20,
			nav:true,
			dots:false,
			mouseDrag:false,
			touchDrag:false,
			navText: ["<i class='icon arrow-left'></i>","<i class='icon arrow-right'></i>"],
			navContainer: "#custom-nav-"+$(item).data("parent-id"),
			onInitialized: owlAfterInit,
			items:1,
	});


	});
	$('.services-photogallery.owl-carousel').on('changed.owl.carousel', function(e) {
		$(this).closest(".services-photo-section").find(".counter").text(++e.item.index + '/' + e.item.count)
	});

	function owlAfterInit(event){
		$(event.currentTarget).closest(".services-photo-section").find(".counter").text('1/' + this.items().length);
		var target = $(event.currentTarget);
		twentyTwentyInit(target.find(".twentytwenty-container"));
	}
	function twentyTwentyInit($containers){
		$containers.twentytwenty({
			before_label: 'До',
			after_label: 'После',
			no_overlay: false,
			move_with_handle_only: true,
			click_to_move: false,
		});
	}
	$(window).resize(function() {
		if(this.resizeTO) clearTimeout(this.resizeTO);
		this.resizeTO = setTimeout(function() {
			$(this).trigger('resizeEnd');
		}, 500);
	});
	$(window).bind('resizeEnd', function() {
		$corousels.each(function(i, item){
			twentyTwentyInit($(item).find(".twentytwenty-container"));
		});
	});
});
