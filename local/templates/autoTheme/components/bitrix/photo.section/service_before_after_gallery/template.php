<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/twenty_twenty/css/twentytwenty.css");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/twenty_twenty/css/twentytwenty-no-compass.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/twenty_twenty/js/jquery.event.move.js");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/twenty_twenty/js/jquery.twentytwenty.js");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.carousel.css");
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/owlcorousel/css/owl.theme.default.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/owlcorousel/js/owl.carousel.js");
$this->addExternalJS("/script.js");
$this->setFrameMode(true);

?>
<div id="custom-nav-<?=$arParams["PARENT_ID"]?>" class="owl-nav">
	<div class="counter"></div>
</div>
<div class="services-photogallery owl-carousel owl-theme" data-parent-id="<?=$arParams["PARENT_ID"]?>">

	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?

		$photoBeforeUtl = CFile::GetPath($arItem["PROPERTIES"]["PHOTO_BEFORE"]["VALUE"]);
		$photoAfterUtl = CFile::GetPath($arItem["PROPERTIES"]["PHOTO_AFTER"]["VALUE"]);
		?>
		<div class="service-photo-item">
			<?if($photoBeforeUtl):?>
				<div class="twentytwenty-container">
					<img src='<?=$photoBeforeUtl?>' style="height:100%">
					<img src='<?=$photoAfterUtl?>' style="height:100%">
				</div>
			<?else:?>
				<img src='<?=$photoAfterUtl?>'>
			<?endif;?>
		</div>
	<?endforeach?>

</div>
