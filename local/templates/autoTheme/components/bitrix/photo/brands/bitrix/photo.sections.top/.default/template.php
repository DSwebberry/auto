<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->addExternalCss("/local/templates/autoTheme/assets/vendor/bxSlider/css/jquery.bxslider.min.css");
$this->addExternalJS("/local/templates/autoTheme/assets/vendor/bxSlider/js/jquery.bxslider.min.js");
$this->addExternalJS("/script.js");



/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="photo-sections-top ">
		<div class="owl-carousel">
			<?foreach($arResult["SECTIONS"] as $arSection):?>
				<?
				$this->AddEditAction('section_'.$arSection['ID'], $arSection['ADD_ELEMENT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "ELEMENT_ADD"), array('ICON' => 'bx-context-toolbar-create-icon'));
				$this->AddEditAction('section_'.$arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
				$this->AddDeleteAction('section_'.$arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_SECTION_DELETE_CONFIRM')));
				?>

					<?foreach($arSection["ROWS"] as $arItems):?>
						<?foreach($arItems as $arItem):?>
							<?if(is_array($arItem)):?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));
								?>
									<?if($arResult["USER_HAVE_ACCESS"]):?>
										<?if(is_array($arItem["PICTURE"])):?>
											<a>
												<img
													border="0"
													src="<?=$arItem["PICTURE"]["SRC"]?>"
													width="<?=$arItem["PICTURE"]["WIDTH"]?>"
													height="<?=$arItem["PICTURE"]["HEIGHT"]?>"
													alt="<?=$arItem["PICTURE"]["ALT"]?>"
													title="<?=$arItem["PICTURE"]["TITLE"]?>"
													/>
											</a>
										<?endif?>
									<?endif?>
							<?endif;?>
						<?endforeach?>
					<?endforeach?>
			<?endforeach;?>
		</div>
</div>
