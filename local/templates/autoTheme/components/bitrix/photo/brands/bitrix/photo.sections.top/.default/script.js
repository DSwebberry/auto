$(document).ready(function(){
	var maxSlides,
		width = $(window).width();

	if (width < 1140 && width > 930) {
		maxSlides = 7;
	}
	else if (width < 930 && width > 715) {
		maxSlides = 5;
	}
	else if (width < 715 && width > 490) {
		maxSlides = 3;
	}
	else if (width < 490) {
		maxSlides = 3;
	}
	else {
		maxSlides = 7;
	}



	var myslider = $('.photo-sections-top .owl-carousel').bxSlider({
		mode:"horizontal",
		slideMargin:40,
		adaptiveHeight:false,
		controls: false,
		pager: false,
		nextText:"",
		prevText:"",
		shrinkItems: true,
		infiniteLoop: true,
		//minSlides:1,
		maxSlides:maxSlides,
		slideWidth:70,
		auto: true,
		autoControls: true,
		stopAutoOnClick: true,
		autoStart: true,
		ticker: true,
		tickerSpeed: 10000,
		speed: 15000,
	});
	// $('.photo-sections-top .owl-carousel').owlCarousel({
	// 	loop:true,
	// 	margin:20,
	// 	nav:false,
	// 	dots:false,
	// 	autoplay:true,
	// 	autoplayTimeout:2000,
	// 	autoplayHoverPause:true,
	// 	responsive:{
	// 		0:{
	// 			items:3
	// 		},
	// 		600:{
	// 			items:7
	// 		},
	// 		1000:{
	// 			items:7
	// 		}
	// 	}
	// });
});
