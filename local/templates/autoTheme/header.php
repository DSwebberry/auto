<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width">
	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?$APPLICATION->SetAdditionalCss("/local/templates/autoTheme/assets/css/styles.css");?>
	<?$APPLICATION->SetAdditionalCss("/local/templates/autoTheme/assets/css/fonts.css");?>
	<?$APPLICATION->SetAdditionalCss("/local/templates/autoTheme/assets/css/font-awesome.css");?>

	<?$APPLICATION->SetAdditionalCss("/local/templates/autoTheme/assets/vendor/select2/css/select2.min.css");?>
	<?$APPLICATION->AddHeadScript("/local/templates/autoTheme/assets/vendor/jquery.min.js");?>
	<?$APPLICATION->AddHeadScript("/local/templates/autoTheme/assets/vendor/select2/js/select2.js");?>

	<?$APPLICATION->AddHeadScript("/local/templates/autoTheme/assets/js/script.js");?>
</head>

<body>


<?$APPLICATION->ShowPanel();?>
<div class="loader bg-grey fixed show"><div class="spin"></div></div>
<div id="wrapper" class="<?=$APPLICATION->GetCurPage() == "/" ? "front-page" : ""?>">
	<div class="left-sidebar-menu">
		<div class="fixed-sidebar">
			<a href="/" class="logo-block">
				<img src="/local/templates/autoTheme/assets/img/logo.png" alt="logo">
			</a>
			<div class="mobile-block">
				<div class="page-title-block">
					<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
						<?=$APPLICATION->ShowTitle()?>
					<? endif; ?>
				</div>
				<div class="menu-control">
					<a class="open-menu">
						<i class="fa fa-bars"></i>
					</a>
				</div>
			</div>

			<div class="menu-block">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"main",
					Array(
						"ROOT_MENU_TYPE" => "top",
						"MAX_LEVEL" => "3",
						"CHILD_MENU_TYPE" => "left",
						"USE_EXT" => "Y"
					)
				);?>
				<div class="additional-block">
					<div class="contacts-block">
						<a href="/kontakty" class="contacts-link">Контакты</a>
						<div class="cta-block">
							<span class="phone">+7 (966) 081-91-81</span>
							<?$APPLICATION->IncludeComponent(
								"altasib:feedback.form",
								"custom",
								array(
									"COMPONENT_TEMPLATE" => "custom",
									"IBLOCK_TYPE" => "altasib_feedback",
									"IBLOCK_ID" => "5",
									"FORM_ID" => "1",
									"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
									"NAME_ELEMENT" => "ALX_DATE",
									"CHECK_ERROR" => "Y",
									"USE_CAPTCHA" => "N",
									"CAPTCHA_TYPE" => "default",
									"NOT_CAPTCHA_AUTH" => "Y",
									"CHANGE_CAPTCHA" => "N",
									"JQUERY_EN" => "jquery",
									"PROPERTY_FIELDS" => array(
										0 => "FIO",
										1 => "PHONE",
										2 => "SERVICE",
									),
									"PROPERTY_FIELDS_REQUIRED" => array(
										0 => "FIO",
										1 => "PHONE",
									),
									"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
									"FB_TEXT_NAME" => "",
									"ALX_LINK_POPUP" => "Y",
									"ALX_NAME_LINK" => "Позвоните мне",
									"ALX_LOAD_PAGE" => "Y",
									"SECTION_FIELDS_ENABLE" => "N",
									"PROPS_AUTOCOMPLETE_NAME" => array(
										0 => "FIO",
									),
									"PROPS_AUTOCOMPLETE_EMAIL" => array(
										0 => "EMAIL",
									),
									"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(
										0 => "PHONE",
									),
									"PROPS_AUTOCOMPLETE_VETO" => "N",
									"MASKED_INPUT_PHONE" => array(
										0 => "PHONE",
									),
									"SECTION_MAIL_ALL" => "ds@webberry.ru",
									"INPUT_APPEARENCE" => array(
										0 => "FORM_INPUTS_LINE",
									),
									"WIDTH_FORM" => "",
									"CATEGORY_SELECT_NAME" => "Выберите категорию",
									"CHECKBOX_TYPE" => "CHECKBOX",
									"COLOR_SCHEME" => "PALE",
									"COLOR_THEME" => "",
									"COLOR_OTHER" => "",
									"POPUP_ANIMATION" => "0",
									"USER_CONSENT" => "N",
									"USER_CONSENT_INPUT_LABEL" => "",
									"USER_CONSENT_ID" => "0",
									"USER_CONSENT_IS_CHECKED" => "Y",
									"USER_CONSENT_IS_LOADED" => "N",
									"BBC_MAIL" => "",
									"MESSAGE_OK" => "Ваше сообщение было успешно отправлено",
									"SHOW_LINK_TO_SEND_MORE" => "N",
									"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
									"ACTIVE_ELEMENT" => "Y",
									"USERMAIL_FROM" => "N",
									"SHOW_MESSAGE_LINK" => "Y",
									"SEND_MAIL" => "N",
									"SEND_IMMEDIATE" => "Y",
									"LOCAL_REDIRECT_ENABLE" => "N",
									"ADD_HREF_LINK" => "Y",
									"POPUP_DELAY" => "0"
								),
								false
							);?>
						</div>
						<div class="address-block">
					<span class="address">
						Москва<br>
						ул. Каланчевская 45
					</span>
							<span class="working-hours">
						Часы работы:<br>
						Пн-Вс: 07:00 - 23:00
					</span>
						</div>
					</div>
					<div class="copyright">
						designed in <a href="http://concep.to/" class="link white-link underlin-false">concep.to</a>
					</div>
				</div>
			</div>



		</div>
	</div>
	<div class="main-content">





